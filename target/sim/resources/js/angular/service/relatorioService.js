angular.module('SIM_APP').service('relatorioService', function () {
    this.generateReport = function (pacienteList) {
        var doc = new jsPDF();

        doc.setTextColor(0, 58, 153);
        doc.setFontSize(22);
        doc.text(5, 20, 'Sistema Integrado Multiprofissional');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(18);
        doc.text(5, 30, 'Lista de pacientes');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(5, 40, 'Nome');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(85, 40, 'Data Nascimento');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(125, 40, 'Prontuário');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(150, 40, 'Técnico de Referência');

        var deslocamento = 50;
        var totalItens = pacienteList.length;
        var pageadd = false;
        var nPage = 0;

        var totalPage = Math.ceil(totalItens / 24);
        console.log(totalPage);

        var j = 0;
        var i = 0;
        pacienteList.forEach(function (e) {
            if (j >= 24 && !pageadd) {
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(170, deslocamento, 'Página ' + ++nPage + ' de ' + totalPage);

                j = 0;
                pageadd = true;
                deslocamento = 50;
                doc.addPage();

                doc.setTextColor(0, 58, 153);
                doc.setFontSize(22);
                doc.text(5, 20, 'Sistema Integrado Multiprofissional');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(18);
                doc.text(5, 30, 'Lista de pacientes');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(5, 40, 'Nome');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(85, 40, 'Data Nascimento');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(125, 40, 'Prontuário');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(150, 40, 'Técnico de Referência');
            }
            pageadd = false;
            j++;
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            doc.text(5, deslocamento, e.nome);

            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            doc.text(85, deslocamento, e.nascimento);

            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            if (e.prontuario) {
                doc.text(125, deslocamento, e.prontuario);
            } else {
                doc.text(125, deslocamento, '');
            }

            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            if (e.tecnicoReferencia) {
                doc.text(150, deslocamento, e.tecnicoReferencia);
            } else {
                doc.text(150, deslocamento, '');
            }


            if (i === totalItens - 1) {
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(170, 280, 'Página ' + ++nPage + ' de ' + totalPage);
            }
            deslocamento += 10;
            i++;
        });
        var data = new Date();
        doc.save('lista-de-pacientes' + data.getDate() + '-' + (data.getMonth() + 1) + '-' + data.getFullYear() + '.pdf');
    };
    
    this.generateReportProfissionais = function (profissionalList) {
        var doc = new jsPDF();

        doc.setTextColor(0, 58, 153);
        doc.setFontSize(22);
        doc.text(5, 20, 'Sistema Integrado Multiprofissional');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(18);
        doc.text(5, 30, 'Lista de profissionais');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(5, 40, 'Nome');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(80, 40, 'Profissão');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(130, 40, 'Telefone');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(165, 40, 'CPF');

        var deslocamento = 50;
        var totalItens = profissionalList.length;
        var pageadd = false;
        var nPage = 0;

        var totalPage = Math.ceil(totalItens / 24);
        console.log(totalPage);

        var j = 0;
        var i = 0;
        profissionalList.forEach(function (e) {
            if (j >= 24 && !pageadd) {
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(170, deslocamento, 'Página ' + ++nPage + ' de ' + totalPage);

                j = 0;
                pageadd = true;
                deslocamento = 50;
                doc.addPage();

                doc.setTextColor(0, 58, 153);
                doc.setFontSize(22);
                doc.text(5, 20, 'Sistema Integrado Multiprofissional');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(18);
                doc.text(5, 30, 'Lista de profissionais');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(5, 40, 'Nome');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(80, 40, 'Profissão');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(130, 40, 'Telefone');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(165, 40, 'CPF');
            }
            pageadd = false;
            j++;
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            doc.text(5, deslocamento, e.nome);

            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            if(e.profissao){
            doc.text(80, deslocamento, e.profissao);                
            }else{
                doc.text(80, deslocamento, '');
            }

            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            if (e.telefone) {
                doc.text(130, deslocamento, e.telefone);
            } else {
                doc.text(130, deslocamento, '');
            }

            doc.setTextColor(0, 0, 0);
            doc.setFontSize(12);
            if (e.cpf) {
                doc.text(165, deslocamento, e.cpf);
            } else {
                doc.text(165, deslocamento, '');
            }


            if (i === totalItens - 1) {
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(170, 280, 'Página ' + ++nPage + ' de ' + totalPage);
            }
            deslocamento += 10;
            i++;
        });
        var data = new Date();
        doc.save('lista-de-profissionais' + data.getDate() + '-' + (data.getMonth() + 1) + '-' + data.getFullYear() + '.pdf');
    };

    this.generateReportPaciente = function (atendimentoList) {
        var doc = new jsPDF();

        doc.setTextColor(0, 58, 153);
        doc.setFontSize(22);
        doc.text(5, 20, 'Sistema Integrado Multiprofissional');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(18);
        doc.text(5, 30, 'Atendimentos paciente ' + atendimentoList[0].paciente.nome);

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(5, 40, 'Profissional');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(85, 40, 'Data');

        doc.setTextColor(0, 0, 0);
        doc.setFontSize(12);
        doc.text(115, 40, 'Apontamentos');

        var deslocamento = 50;
        atendimentoList.forEach(function (e) {
            if (deslocamento < 290) {
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(5, deslocamento, e.profissional.nome);

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(85, deslocamento, e.dataApontamento);

                var splitTitle = doc.splitTextToSize(e.apontamento, 90);
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                splitTitle.forEach(function (i) {
                    doc.text(115, deslocamento, i);
                    deslocamento += 10;
                });
                deslocamento += 10;
            } else {
                //addPage
                deslocamento = 50;
                doc.addPage();

                doc.setTextColor(0, 58, 153);
                doc.setFontSize(22);
                doc.text(5, 20, 'Sistema Integrado Multiprofissional');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(18);
                doc.text(5, 30, 'Atendimentos paciente ' + atendimentoList[0].paciente.nome);

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(5, 40, 'Profissional');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(85, 40, 'Data');

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(115, 40, 'Apontamentos');
                
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(5, deslocamento, e.profissional.nome);

                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                doc.text(85, deslocamento, e.dataApontamento);

                var splitTitle = doc.splitTextToSize(e.apontamento, 90);
                doc.setTextColor(0, 0, 0);
                doc.setFontSize(12);
                splitTitle.forEach(function (i) {
                    doc.text(115, deslocamento, i);
                    deslocamento += 10;
                });
                deslocamento += 10;
            }
        });

        var data = new Date();
        doc.save('atendimentos-' + data.getDate() + '-' + (data.getMonth() + 1) + '-' + data.getFullYear() + '.pdf');
    };
});

