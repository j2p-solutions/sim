<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="SIM_APP">
    <head>
        <!-- Incluindo a p�gina que cont�m os arquivos css comuns para as p�ginas-->
        <jsp:include page="/resources/templates/base-style.jsp"/>
        <link href="<c:url value="/resources/css/profissional/app.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/table/responsive-table.css"/>" rel="stylesheet">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body class="grey lighten-2" ng-controller="ProfissionalController as controller" ng-init="controller.init()">
        <!-- MODAL LOADING -->
        <div class="modal" id="modal-loading">
            <div class="modal-content">
                <div class="preloader-wrapper big active">
                    <div class="spinner-layer spinner-blue-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <h5>Carregando...</h5>
            </div>            
        </div>
        <!-- Incluindo a JSP que cont�m a navbar do usu�rio Profissional -->
        <nav>
            <div class="nav-wrapper">
                <a href="#" class="brand-logo">
                    <img class="responsive-img logo" width="170" height="150" src="<c:url value="/resources/img/logo-sim.png"/>" />                             
                </a>                
            </div>
        </nav>

        <div class="container">
            <h2>Ola, primeiramente selecione a institui��o que voc� deseja acessar:</h2>           
            <div class="row" ng-repeat="instituicao in controller.instituicaoListProfissional">
                <div>                    
                    <button class="btn btn-large blue darken-3" ng-click="controller.setInstituicaoSession(instituicao)">{{instituicao.nome}}</button>
                </div>
            </div>
        </div>      
        <jsp:include page="/resources/templates/footer.jsp"/>   
        <!--Incluindo a jsp que cont�m os arquivos JS comuns para as p�ginas-->
        <jsp:include page="/resources/templates/base-script.jsp"/>  
        <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/angular/profissional/profissional-controller.js"/>"></script>
        <script>
                            $(function () {
                                $(".button-collapse").sideNav();
                            });
        </script>
    </body>
</html>
