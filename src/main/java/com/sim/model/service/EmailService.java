/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sim.model.service;

import com.sim.model.base.service.BaseEmailService;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author Joao
 */
public class EmailService implements BaseEmailService {

    @Override
    public void sendEmail(String destino, String assunto, String texto) throws Exception {
        try {
            Email email = new SimpleEmail();
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(587);
            email.setStartTLSRequired(true);
            //email.setSSLOnConnect(true);
            email.setAuthenticator(new DefaultAuthenticator("joaopedro.j2p@gmail.com", "jp061195"));
            email.setFrom("joaopedro.j2p@gmail.com");
            email.addTo(destino);
            email.setSubject(assunto);
            email.setMsg(texto);
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
            throw e;
        }
    }
}