﻿
CREATE TABLE instituicao(
	usuario_fk bigint not null,
	conf_versao varchar(4),
	telefone varchar(20),
	endereco varchar(60),
	numero varchar(10),
	bairro varchar(60),
	cidade varchar(60),
	uf varchar(2),
	cep varchar(10),
	primary key(usuario_fk)
);

create table paciente(
	id bigserial not null,
        instituicao_fk bigint not null,
	nome varchar(60) not null,
	nascimento date,
	cpf varchar(16),
	rg varchar(15),
	nome_pai varchar(60),
	nome_mae varchar(60),
	endereco varchar(60),
	numero varchar(10),
	bairro varchar(60),
	cidade varchar(60),
	uf varchar(2),
	cep varchar(10),
	telefone varchar(20),
	obs varchar(200),
        prontuario varchar(200),
        tecnico_referencia varchar(60),
	primary key(id)
);

create table atendimento(
	id bigserial not null,
	paciente_fk bigint not null,
	profissional_fk bigint not null,
	data_apontamento date not null,
	apontamento varchar(400) not null,
	primary key(id)
);

create table usuario(
	id bigserial not null,
	nome varchar(60) not null,
	email varchar(60) not null unique,
	senha varchar(60) not null,
	primary key(id)
);

create table master(
	usuario_fk bigint not null,
	primary key(usuario_fk)
);

create table profissional(
	usuario_fk bigint not null,
	instituicao_fk bigint not null,
	profissao varchar(60),
	registro_de_conselho varchar(40),
	cpf varchar(16),
	rg varchar(15),
	endereco varchar(60),
	numero varchar(10),
	bairro varchar(60),
	cidade varchar(60),
	uf varchar(2),
	cep varchar(10),
	telefone varchar(20),
	primary key(usuario_fk)
);

create table mensagem(
	id bigserial not null,
	destinatario_fk bigint not null,
	remetente_fk bigint not null,
	conteudo varchar(200),
	data_hora_envio timestamp without time zone NOT NULL,
	data_hora_leitura timestamp without time zone,
        is_lida boolean default false,
	primary key(id)
);

create table profissional_paciente(
        profissional_fk bigint not null,
        paciente_fk bigint not null,
        primary key(profissional_fk, paciente_fk)
);

create table recuperacao_senha(
    id bigserial not null,
    hash varchar(6) unique not null,
    usuario_fk bigint not null,
    ativo boolean not null default true,
    primary key(id)
);

alter table recuperacao_senha add constraint recuperacao_senha_usuario_fk
foreign key(usuario_fk) references usuario(id)
on update cascade on delete cascade;

alter table profissional_paciente add constraint profissional_paciente_profissional_fk
foreign key(profissional_fk) references profissional(usuario_fk)
on update cascade on delete cascade;

alter table profissional_paciente add constraint profissional_paciente_paciente_fk
foreign key(paciente_fk) references paciente(id)
on update cascade on delete cascade;

alter table atendimento add constraint atendimento_paciente_fk
foreign key(paciente_fk) references paciente(id)
on update cascade on delete cascade;

alter table atendimento add constraint atendimento_profissional_fk
foreign key(profissional_fk) references profissional(usuario_fk)
on update cascade on delete cascade;

alter table profissional add constraint profissional_instituicao_fk
foreign key(instituicao_fk) references instituicao(usuario_fk)
on update cascade on delete cascade;

alter table profissional add constraint profissional_usuario_fk
foreign key(usuario_fk) references usuario(id)
on update cascade on delete cascade;

alter table mensagem add constraint mensagem_remetente_fk
foreign key(remetente_fk) references profissional(usuario_fk)
on update cascade on delete cascade;

alter table mensagem add constraint mensagem_destinatario_fk
foreign key(destinatario_fk) references profissional(usuario_fk)
on update cascade on delete cascade;

alter table instituicao add constraint instituicao_usuario_fk
foreign key(usuario_fk) references usuario(id)
on update cascade on delete cascade;

alter table master add constraint master_usuario_fk
foreign key(usuario_fk) references usuario(id)
on update cascade on delete cascade;

alter table paciente add constraint paciente_instituicao_fk
foreign key(instituicao_fk) references instituicao(usuario_fk)
on update cascade on delete cascade;

--Criação do usuário Master, para acesso ao sistema
--Credenciais para acesso:
--Email: master@gmail.com
--Senha: 123
INSERT INTO usuario(nome, email, senha)VALUES ('Master', 'master@gmail.com', '202cb962ac59075b964b07152d234b70');
INSERT INTO master(usuario_fk)VALUES ((select id from usuario where email = 'master@gmail.com'));
